<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendance;
use App\member;
use Excel;

class AttendanceController extends Controller
{
    public function createAttendance(Request $request){
        $data=$this->validate($request, [
            'p_name'=>'required',
            'date'=>'required',
            'time'=>'required',
            'mark'=>'required'
        ]);
        $n = new Attendance();
        $n->p_name=$request['p_name'];
        $n->date=$request['date'];
        $n->time=$request['time'];
        $n->mark=$request['mark'];
        if($n->save()){
            return response()->json([
                'status'=>'success',
                'msg'=>'Attandance Saved'
            ]);
        }else{
            return response()->json([
                'status'=>'error',
                'msg'=>'Attandance not Saved, Pls Try Again'
            ]);
        }
    }
    public function getAttendance(){
        $a = Attendance::orderBy('created_at', 'desc')->get();
        $d=[];
        foreach ($a as $key => $value) {
            $e=explode(',', $value->mark);
            $c_no = count($e); //current total number of members and workers
            
            $data = array_map(function($ar){
                $b=explode(':',$ar);
                return $b;
            }, $e);
            $pre=0;
            $mem=0;
            $mr=[];
            for ($i=0; $i < count($data); $i++) { 
                
                if ($data[$i][1] == "1") {
                    $pre++;
                    $mb=member::find($data[$i][0]);
                    // $mr[$i]=$mb->unit;
                    // echo ($mb['unit']);
                    
                    if($mb['unit'] == 'member'){
                        $mem++;
                    }
                }
            }
            // dd($mr);
            $v=[
                'all'=>$c_no,
                'present'=>$pre,
                'm_present'=>$mem
            ];
            $d[$key] = $v;
        }
        
        //  dd($d);
         return response()->json(['check'=>$d, 'attendance'=>$a], 200);
        
    }
    public function deleteAttendance($id){
        if(Attendance::destroy($id)){
            return response()->json([
                'status'=>'success',
                'msg'=>'Attendance Delete Successfully'
            ]);
        }else{
            return response()->json([
                'status'=>'error',
                'msg'=>'Error Deleting Attendance, Pls, Try again'
            ]);
        }
    }
    
    public function showAttendance($id){
        $a= Attendance::find($id);
        $e=explode(',', $a->mark);
        //  this get the id from the mark column and finds it data in from the members table. After then it set the status to the present/marked status sent and then send it to the frontend 
        $data = array_map(function($ar){
            $b=explode(':',$ar);
            return $b;
        }, $e);
        $member=[];
        for ($i=0; $i < count($data); $i++) {
            if ($data[$i][1] == "1") {
                $mb=member::find($data[$i][0]);
                $mb['status']=$data[$i][1];
                $member[$i]=$mb;
            }
        }
        // dd($member);
        return response()->json([
            'members'=>$member,
            'program'=>$a
        ]);
    }

    public function print($member){
        $foster=foster::find(2);
        $data=[
            'foster'=>$foster
        ];
        // dd($foster);
        // $pdf=\PDF::loadView('user', $foster);
        $pdf = PDF::loadView('export',$data);  
        return $pdf->download('medium.pdf');
    }

    public function export($id,$category){
        
        $a= Attendance::find($id);
        $e=explode(',', $a->mark);
        //  this get the id from the mark column and finds it data in from the members table. After then it set the status to the present/marked status sent and then send it to the frontend 
        $data = array_map(function($ar){
            $b=explode(':',$ar);
            return $b;
        }, $e);
        $member=[];
        switch ($category) {
            case 'present':
                for ($i=0; $i < count($data); $i++) {
                    if ($data[$i][1] == "1") {
                        $mb=member::find($data[$i][0])->toArray();
                        $mb['status']=$data[$i][1];
                        $member[$i]=$mb;
                    }
                }
                break;
            case 'worker-present':
                for ($i=0; $i < count($data); $i++) {
                    if ($data[$i][1] == "1") {
                        $mb=member::find($data[$i][0])->toArray();
                        $mb['status']=$data[$i][1];
                        if($mb['unit'] != 'member'){
                            $member[$i]=$mb;
                        }
                    }
                }
                break;
            case 'member-present':
                for ($i=0; $i < count($data); $i++) {
                    if ($data[$i][1] == "1") {
                        $mb=member::find($data[$i][0])->toArray();
                        $mb['status']=$data[$i][1];
                        if($mb['unit'] == 'member'){
                            $member[$i]=$mb;
                        }
                    }
                }
                break;
            case 'worker-absent':
                for ($i=0; $i < count($data); $i++) {
                    if ($data[$i][1] == "0") {
                        $mb=member::find($data[$i][0])->toArray();
                        $mb['status']=$data[$i][1];
                        if($mb['status'] != 'member'){
                            $member[$i]=$mb;
                        }
                    }
                }
                break;
            case 'member-absent':
                for ($i=0; $i < count($data); $i++) {
                    if ($data[$i][1] == "0") {
                        $mb=member::find($data[$i][0])->toArray();
                        $mb['status']=$data[$i][1];
                        if($mb['status'] == 'member'){
                            $member[$i]=$mb;
                        }
                    }
                }
                break;
            case 'absent':
                for ($i=0; $i < count($data); $i++) {
                    if ($data[$i][1] == "0") {
                        $mb=member::find($data[$i][0])->toArray();
                        $mb['status']=$data[$i][1];
                        $member[$i]=$mb;
                    }
                }
                break;
            default:
                for ($i=0; $i < count($data); $i++) {
                    $mb=member::find($data[$i][0])->toArray();
                    $mb['status']=$data[$i][1];
                    $member[$i]=$mb;
                }
                break;
        }
        // dd($member);
        $mem[]=array('Name','Email','Level','Phone','Unit','Department','D.O.B','Address','Status');
        foreach ($member as $value) {
            $mem[]=array(
                'Name'=>$value['name'],
                'Email'=>$value['email'],
                'Level'=>$value['level'],
                'Phone'=>$value['phone'],
                'Unit'=>$value['unit'],
                'Department'=>$value['dept'],
                'D.O.B'=>$value['dob'],
                'Address'=>$value['address'],
                'Status'=>$value['status'] == 0?'Absent':'Present'
            );
        }
        //dd($mem);
        Excel::create('attendance_'.$a->date.'_'. $category,function($excel) use($mem){
            $excel->setTitle('TWC list');
            $excel->sheet('Attendance list', function($sheet)
                use ($mem){
                $sheet->fromArray($mem,null,'A1',false,false);
            });
        })->download('xlsx',['Access-Control-Allow-Origin' => '*']);
        
    }
}
