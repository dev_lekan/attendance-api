<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\member;
use Excel;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return member::orderBy('name','asc')->get();
    }
    public function memberOnly(){
        return member::where('unit','member')->orderBy('name','asc')->get();
    }

    public function count(){
        $mem=member::where('unit','member')->count();
        $w=member::where('unit','!=', 'member')->count();
        return response()->json([
            'member'=>$mem,
            'worker'=>$w
        ],200);
    }
    public function store(Request $request)
    {
        //use form-datawhen testing with postman for create
        $data = $this->validate($request, [
            'name'=>'required|string|unique:members',
            'email'=>'email|unique:members',
            'level'=>'required',
            'phone'=>'required',
            'unit'=>'required|string',
            'dept'=>'required|string',
            'dob'=>'required',
            'address'=>'required',
            'status'=>'required',
        ]);
        if(member::create($data)){
            return response()->json([
                'status'=> 'success',
                'msg'=>'Member Added Successfully',
            ]);
        }
        return response()->json([
            'status'=> 'error',
            'msg'=>'Error adding Member, Pls Try Again',
        ]);
    }

    public function show($id)
    {
        return member::find($id);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //use x-www-form-urlencoded when testing with postman for update
        $data = $this->validate($request, [
            'name'=>'required|string',
            'level'=>'required',
            'phone'=>'required',
            'dept'=>'required|string',
            'dob'=>'required',
            'address'=>'required',
            'unit'=>'required',
            'email'=>'email|unique:members'
        ]);
        
        $m = member::find($id);
        if($m->update($data)){
            return response()->json([
                'data'=>$m,
                'status'=> 'success',
                'msg'=>'Update Successful',
            ]);
        }else{
            return response()->json([
                'status'=> 'error',
                'msg'=>'Update Error, Pls Try Again',
            ]);
        }

    }

    public function addWorker(Request $request, $id){
        $this->validate($request, [
            'unit'=>'required'
        ]);
        $m=member::find($id);
        $m->unit = $request['unit'];
        if($m->save()){
            return response()->json([
                'data'=>$m,
                'status'=> 'success',
                'msg'=>'Worker Added Successfully',
            ]);
        }
    }

    public function destroy($id)
    {
        if(member::destroy($id)){
            return response()->json([
                'status'=> 'success',
                'msg'=> 'Delete Successful'
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'msg' => 'An error Occur When Deleting, Please Try Again'
            ]);
        }
    }

    public function export($category){
        
        $member=[];
        switch ($category) {
            case 'members':
                $member=member::where('unit','member')->get()->toArray();
                break;
            case 'workers':
                $member=member::where('unit','!=','member')->get()->toArray();
                break;
            default:
                $member=member::all()->toArray();
                break;
        }
        // dd($member);
        $mem[]=array('Name','Level','Email','Phone','Unit','Department','D.O.B','Address');
        foreach ($member as $value) {
            $mem[]=array(
                'Name'=>$value['name'],
                'Level'=>$value['level'],
                'Email'=>$value['email'],
                'Phone'=>$value['phone'],
                'Unit'=>$value['unit'],
                'Department'=>$value['dept'],
                'D.O.B'=>$value['dob'],
                'Address'=>$value['address']
            );
        }
        
        Excel::create('TWC_'.$category.'_'.date('d_M_Y').'_list',function($excel) use($mem){
            $excel->setTitle('TWC list');
            $excel->sheet('TWC list', function($sheet)
                use ($mem){
                $sheet->fromArray($mem,null,'A1',false,false);
            });
        })->download('xlsx',['Access-Control-Allow-Origin' => '*']);
    }

    public function addLevel(){
        $all=member::all();
        foreach($all as $key => $val){
            switch ($val->level) {
                case 'jambite':
                    $all[$key]->level = 100;
                    break;
                case 'pdite':
                    $all[$key]->level = 100;
                    break;
                case '100':
                    $all[$key]->level = 200;
                    break;
                case '200':
                    $all[$key]->level = 300;
                    break;
                case '300':
                    $all[$key]->level = 400;
                    break;
                case '400':
                    $all[$key]->level = 500;
                    break;
                case '500':
                    $all[$key]->level = 'graduate';
                    break;
                case 'graduate':
                    $all[$key]->level = 'graduates';
                    break;
                default:
                    break;
            }
            $all[$key]->save();
        }
        
        return response()->json([
            'status'=>'success',
            'msg'=>'Level added successfully'
        ],200);
    }
    
    public function minusLevel(){
        $all=member::all();
        foreach($all as $key => $val){
            switch ($val->level) {
                case '100':
                    $all[$key]->level = 'jambite';
                    break;
                case '200':
                    $all[$key]->level = 100;
                    break;
                case '300':
                    $all[$key]->level = 200;
                    break;
                case '400':
                    $all[$key]->level = 300;
                    break;
                case '500':
                    $all[$key]->level = 400;
                    break;
                case 'graduate':
                    $all[$key]->level = 500;
                    break;
                case 'graduates':
                    $all[$key]->level = 'graduate';
                    break;
                default:
                    break;
            }
            $all[$key]->save();
        }
        
        return response()->json([
            'status'=>'success',
            'msg'=>'A level minus successfully'
        ],200);
    }
}


