<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\member;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return member::where('unit', '!=', 'member')->orderBy('name','asc')->get();
    }

    public function removeWorker($id){
        $m = member::find($id);
        $m->unit = 'member';
        if(!$m->save()){
            return response()->json([
                'status'=>'error',
                'msg'=>'Removing Worker Unsuccessful'
            ]);
        }

        return response()->json([
            'data'=>$m,
            'status'=>'success',
            'msg'=>'Worker Removed Successfully'
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name'=>'required|string',
            'level'=>'required',
            'phone'=>'required',
            'unit'=>'required|string',
            'dept'=>'required|string',
            'dob'=>'required',
            'address'=>'required'
        ]);
        $msg=[
            'status'=> '',
            'msg'=>'',
        ];
        if(member::create($data)){
            $msg=[
                'status'=> 'success',
                'msg'=>'Worker Added Successfully',
            ];
            return json_encode($msg);
        }
        $msg=[
            'status'=> 'error',
            'msg'=>'Error adding Worker, Pls Try Again',
        ];
        return json_encode($msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg=[
            'status'=>'',
            'msg'=>''
        ];
        if(member::destroy($id)){
            $msg['status'] = 'success';
            $msg['msg'] = 'Worker Deleted Successful';
            return json_encode($msg);
        }
        $msg['status'] = 'error';
        $msg['msg'] = 'An error Occur When Deleting, Please Try Again';
        return json_encode($msg);
    }
}
