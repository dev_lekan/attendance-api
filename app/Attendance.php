<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public function setTimeAttribute($value){
        $this->attributes['time']=date("g:i a", strtotime($value));
    }
    public function setDateAttribute($value){
        $date = date_create($value);
        $this->attributes['date']=date_format($date,"D, d M Y");
    }
}
