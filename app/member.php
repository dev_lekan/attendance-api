<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class member extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    public function setDobAttribute($value){
        $date = date_create($value);
        $this->attributes['dob']=date_format($date,"D, d M Y");
    }
}
