<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/lekan', function(){
	return 'lekan test run';
});
Route::prefix('/v1')->group(function(){
    Route::resource('/member', 'MemberController');
    Route::get('/member-only', 'MemberController@memberOnly');
    Route::get('/remove-worker/{id}', 'WorkerController@removeWorker');
    Route::patch('/addWorker/{id}', 'MemberController@addWorker');
    Route::post('mark','AttendanceController@createAttendance');
    Route::get('attendance','AttendanceController@getAttendance');
    Route::delete('attendance/delete/{id}', 'AttendanceController@deleteAttendance');
    Route::get('attendance/show/{id}', 'AttendanceController@showAttendance');
    Route::get('export/{category}', 'MemberController@export');
    Route::get('export-attendance/{id}/{category}','AttendanceController@export');
    Route::get('count', 'MemberController@count');
    // Route::get()
    Route::resource('/worker', 'WorkerController');
    Route::get('addLevel','MemberController@addLevel');
    Route::get('minusLevel','MemberController@minusLevel');
});

